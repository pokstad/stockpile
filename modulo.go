/*Package modulo provides a static site compiler for serving Go modules as
described in the `go help goproxy` documentation:

	GET $GOPROXY/<module>/@v/list returns a list of all known versions of the
	given module, one per line.

	GET $GOPROXY/<module>/@v/<version>.info returns JSON-formatted metadata
	about that version of the given module.

	GET $GOPROXY/<module>/@v/<version>.mod returns the go.mod file
	for that version of the given module.

	GET $GOPROXY/<module>/@v/<version>.zip returns the zip archive
	for that version of the given module.

Modulo achieves this by doing the following:

	1. download Go project module dependencies
	2. copy unarchived dependencies from module cache to temp folder
	3. archive the temp folder as a GitLab CI artifact
	4. creates HTML redirect files mapping the expected GOPROXY paths to the
	corresponding artifact API URL
	5. merges the new list of HTML files with existing ones in the pages branch

Modulo comes with a helper command to add the necessary changes to your GitLab
CI file (.gitlab-ci.yml). These are a great starting point for your custom CI
pipelines. It includes jobs for downloading and archiving dependencies, and also
enables a project wide cache for the Go modules cache to prevent unnecessary
downloading.

*/
package modulo

import (
	"context"
	"os/exec"
)

func main() {

}

func modDownload(ctx context.Context) *exec.Cmd {
	return exec.CommandContext(ctx, "go", "mod", "download")
}

func archivedModules(ctx *context.Context) *exec.Cmd {
	return nil
}
